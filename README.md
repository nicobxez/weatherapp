# Weather App 🌈
---
My first project in React JS with the aim of learning to use this tool, this project is about the use of a climate API to offer its services.

## Starting 🚀
---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### What does the project look like?

![indexImage](https://bitbucket.org/nicobxez/weatherapp/raw/3145bee02c7cbf28d0e66a9879bad902aabe8598/src/components/readmeImages/home.png)

## Pre-requirements 📋
---

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Built with 🛠️
---

I'm using these powerful libraries

* Weather API - https://openweathermap.org/api
* Material UI - https://material-ui.com/

---
⌨️ with ❤️ by [nicobxez](https://bitbucket.org/nicobxez/) 😊